'use strict';
const log = require('log4js');
log.configure({
    appenders: { rekap: { type: "file", filename: "rekap.log" } },
    categories: { default: { appenders: ["rekap"], level: "error" } }
});

const logger = log.getLogger('rekap');

exports.send = function(username,success, message, result, option) {

    var data = {
        'success': success,
        'message': message,
        'total':result.length,
        'result': result
    };

    try{
        // console.log(username,new Date()," --> " + JSON.stringify(data));
        this.logging(username,new Date()," --> " + JSON.stringify(data));
    }catch (e){

    }

    console.log('SEND RESPONSE');
    option.json(data);
    option.end();
};

exports.logging = function (username,time,log,type=null){
    if(type === null){
        logger.level = "info";
        logger.info("{'username':'"+username+"','log':'"+log+"'}");
    }
}
