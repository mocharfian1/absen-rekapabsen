'use strict';
var multer  = require('multer');
var path = require('path');
var upload = multer(
    {
        dest : 'uploads/',
        fileFilter : function(req, file , inst){

            var extFile = path.extname(file.originalname);
            console.log("OKE");
            if(extFile !== ".jpg"){
                // skip uploadnya
                inst(null, false)
            } else {
                inst(null, true)
            }
        }
    }
);

module.exports = function(app) {
    var controller = require('./controller');

    app.route('/')
        .get(controller.index);

    app.route('/getRekapAbsen')
        .get(controller.getRekapAbsen);

    app.route('/getRekapKegiatan')
        .get(controller.getRekapKegiatan);

    app.post('/submitKegiatan',upload.single("foto_kegiatan"),controller.submitKegiatan);
};