'use strict';

var config = {
    photo_dir:{
        images_kegiatan_dir:'images/',
        images_kegiatan_thumb_dir:'images_thumb/'
    }
}

exports.config = function(){
    return config;
}