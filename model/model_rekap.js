'use strict';
var connection = require('../conn');
const moment = require('moment-timezone');
var saveImage = require('fs');
const conf = require('../config');
const { photo_dir } = conf.config();

exports.getAbsenPegawai = function (nip=null,date_start=null, date_end=null,current_month=null,isDL=null){
    return new Promise((resolve,reject) =>{
        var filterDL = "";
        if(isDL === "true"){
            filterDL = "and type_absen='DL'";
        }

        if(parseInt(current_month) === 1){
            var query = "SELECT * FROM maps_absen WHERE nip='"+nip+"' AND MONTH(absen_masuk) = MONTH(NOW()) and Year(insert_date)=year(now()) "+filterDL;
        }else{
            var query = "SELECT * FROM maps_absen WHERE nip='"+nip+"' AND DATE(absen_masuk) BETWEEN DATE('"+date_start+"') AND DATE('"+date_end+"') "+filterDL;
        }
        connection.query(query, (err,result,fields)=>{
            if(err){
                return reject("[MDRekap 001] "+err);
            }else{                
                if(result.length > 0){
                    return resolve(result);
                }else{
                    return reject("[MDRekap 002] Tidak ada data");
                }
            }
        });
    });
}

exports.getKegiatan = function (nip=null,date_start=null, date_end=null,current_month=null,insert_date=null){
    return new Promise((resolve, reject) => {
        if(insert_date === null) {
            if (parseInt(current_month) === 1) {
                var query = "SELECT * FROM maps_kegiatan WHERE nip='" + nip + "' AND DATE(insert_date) = DATE(NOW())";
            } else {
                var query = "SELECT * FROM maps_kegiatan WHERE nip='" + nip + "' AND DATE(insert_date) BETWEEN DATE('" + date_start + "') AND DATE('" + date_end + "')";
            }
        }else{
            var query = "SELECT * FROM maps_kegiatan WHERE nip='" + nip + "' AND DATE(insert_date) = DATE('"+insert_date+"')";
        }

        connection.query(query, (err,result)=>{
            if(err){
                return reject("[MDRekapKegiatan 002] "+err);
            }else{
                if(result.length > 0){
                    return resolve(result);
                }else{
                    return reject("[MDRekapKegiatan 003] Data tidak ditemukan.");
                }
            }
        });
    });
}

exports.submitKegiatan = function(nip=null,arrField=null){
    return new Promise((resolve, reject)=>{
        if(arrField != null){
            let nama_file = arrField.device_id + "_" + (new Date().getTime());
            var foto = arrField.foto_kegiatan;
            var foto_thumb = arrField.foto_kegiatan_thumb;
            var foto_name = arrField.foto_name;
            var thumb_name = arrField.thumb_name;

            this.saveImageToFile(nama_file,foto,foto_thumb,foto_name,thumb_name).then((res)=>{
                var id_kegiatan = arrField.id_kegiatan;

                var query = "";
                if(id_kegiatan === ""){
                    query = "INSERT INTO maps_kegiatan (session_token, nip, id_jenis_kegiatan, judul, foto_kegiatan, foto_kegiatan_thumb, keterangan, berkas, insert_by, insert_date, update_by, update_date) VALUES ('" + arrField.token + "', '" + nip + "', '1', '" + arrField.judul + "', '" + res.path_foto + "', '" + res.path_thumb + "', '" + arrField.keterangan + "', NULL, '1', '" + moment().tz("Asia/Jakarta").format('YYYY-MM-DD HH:mm:ss') + "', '1', '" + moment().tz("Asia/Jakarta").format('YYYY-MM-DD HH:mm:ss') + "')";
                }else {
                    if(res.path_foto === ""){
                        query = "UPDATE maps_kegiatan SET JUDUL='"+ arrField.judul +"', keterangan='"+arrField.keterangan+"' where id="+id_kegiatan;
                    }else{
                        query = "UPDATE maps_kegiatan SET JUDUL='"+ arrField.judul +"', keterangan='"+arrField.keterangan+"', foto_kegiatan='"+res.path_foto+"', foto_kegiatan_thumb='" + res.path_thumb + "' where id="+id_kegiatan;
                    }
                }

                connection.query(query, (err, result) => {
                    if (err == null) {
                        return resolve({success: true, result: result});
                    } else {
                        return reject(err);
                    }
                });

            }).catch((err)=>{
                return reject(err);
            });
        }else{
            return reject("Bad Credentials.");
        }
    });

}

exports.saveImageToFile = function (nama_foto,file_foto,file_thumb,foto_name,thumb_name){
    return new Promise((resolve, reject)=>{
        var thumb = photo_dir.images_kegiatan_thumb_dir + thumb_name;
        var foto = photo_dir.images_kegiatan_dir + foto_name;

        if(foto_name === "" || foto_name === null){
            return resolve({success:true,path_thumb:"",path_foto:""});
        }else{
            
            // console.log(foto);
            saveImage.writeFile(thumb, file_thumb, {encoding: 'base64'}, function(errThumb) {
                if(errThumb == null){
                    saveImage.writeFile(foto, file_foto, {encoding: 'base64'}, function(errFoto) {
                        if(errFoto == null){
                            return resolve({success:true,path_thumb:thumb,path_foto:foto});
                        }else{
                            return reject(errFoto);
                        }
                    });
                }else{
                    return reject(errThumb);
                }
            });
        }
    });
}

exports.getToleransi = function(){
    return new Promise((resolve,reject)=>{
        var query = "select * from maps_pola_toleransi order by denda_persen asc";
        connection.query(query,(err,result)=>{
            if(err){
                return reject(err);
            }else{
                if(result.length > 0){
                    return resolve(result);
                }else{
                    return reject("Tidak ada data");
                }
            }
        });
    });
}