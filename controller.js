'use strict';

const clearTimeZone = require('moment');
const moment = require('moment-timezone');
var connection = require('./conn');
var response = require('./res');
const request = require('request').defaults({rejectUnauthorized:false});
const CurDate = moment().tz("Asia/Jakarta").format('YYYY-MM-DD HH:mm:ss');
const CurTime = moment().tz("Asia/Jakarta").format('HH:mm:ss');
const CurDateOnly = moment().tz("Asia/Jakarta").format('YYYY-MM-DD');
var fs = require('fs');

var MDSession = require('./model/model_session');
var MDRekap = require('./model/model_rekap');

exports.index = function(req, res) {
    response.send("Hello from the Node JS RESTful side!", res)
};

exports.getRekapAbsen = function (req,res){
    const { headers } = req;
    var params = {
        token:headers['x-token-access'],
        device_id:headers['x-device-id'],
        date_start:req.query.date_start,
        date_end:req.query.date_end,
        current_month:req.query.now,
        isDL:req.query.is_dinas_luar
    }

    MDSession.getSessionInfo(params.token).then((SessionData)=>{
        MDRekap.getAbsenPegawai(SessionData.nip,params.date_start,params.date_end,params.current_month,params.isDL).then((RekapData)=>{
            MDRekap.getToleransi().then((ToleransiData)=>{
                var iRekap = 0;
                while(RekapData[iRekap]){

                    let Rekap_jamMasuk = moment(RekapData[iRekap].absen_masuk).tz('Asia/Jakarta').format("YYYY-MM-DD") + " " + RekapData[iRekap].jam_masuk;
                    let Rekap_jamPulang = moment(RekapData[iRekap].absen_pulang).tz('Asia/Jakarta').format("YYYY-MM-DD") + " " + RekapData[iRekap].jam_pulang;

                    let TimeMasuk = moment(Rekap_jamMasuk,"YYYY-MM-DD HH:mm:ss").tz('Asia/Jakarta');
                    let TimePulang = moment(Rekap_jamPulang,"YYYY-MM-DD HH:mm:ss").tz('Asia/Jakarta').valueOf();
                    let TimeAbsenMasuk = moment(RekapData[iRekap].absen_masuk).tz('Asia/Jakarta').valueOf();
                    let TimeAbsenPulang = moment(RekapData[iRekap].absen_pulang).tz('Asia/Jakarta').valueOf();

                    let msMasuk = TimeAbsenMasuk - TimeMasuk;
                    let msPulang = TimePulang - TimeAbsenPulang;

                    let minuteMasuk = (msMasuk / 1000) / 60;
                    let minutePulang = (msPulang / 1000) / 60;

                    if(RekapData[iRekap].absen_masuk == null){
                        let tipeToleransiMasuk = ToleransiData.filter(tolData => tolData.jenis_kategori === 'MASUK');
                        
                        var dendaMasuk = Math.max.apply(Math,tipeToleransiMasuk.map(function(d){
                            return d.denda_persen;
                        }));
                    }else{
                        let tipeToleransiMasuk = ToleransiData.filter(tolData => tolData.jenis_kategori === 'MASUK');
                        let arrDendaMasuk = tipeToleransiMasuk.filter(tipeTolData => tipeTolData.lebih_dari < minuteMasuk);
                        
                        if(arrDendaMasuk.length > 0){
                            var dendaMasuk = Math.max.apply(Math,arrDendaMasuk.map(function(d){
                                return d.denda_persen;
                            }));
                        }else{
                            var dendaMasuk = 0;
                        }
                        
                    }
                    

                    if(RekapData[iRekap].absen_pulang == null){
                        let tipeToleransiPulang = ToleransiData.filter(tolData => tolData.jenis_kategori === 'PULANG');
                
                        var dendaPulang = Math.max.apply(Math,tipeToleransiPulang.map(function(d){
                            return d.denda_persen;
                        }));
                    }else{
                        let tipeToleransiPulang = ToleransiData.filter(tolData => tolData.jenis_kategori === 'PULANG');
                        let arrDendaPulang = tipeToleransiPulang.filter(tipeTolData => tipeTolData.kurang_dari < minutePulang);
                        
                        if(arrDendaPulang.length > 0){
                            var dendaPulang = Math.max.apply(Math,arrDendaPulang.map(function(d){
                                return d.denda_persen;
                            }));
                        }else{
                            var dendaPulang = 0;
                        }
                    }
                    

                    let timeInsertDate = moment(RekapData[iRekap].insert_date,"YYYY-MM-DD").tz('Asia/Jakarta').valueOf();
                    let timeCurrentDate = moment(CurDateOnly,"YYYY-MM-DD").tz('Asia/Jakarta').valueOf();
                    if(timeCurrentDate === timeInsertDate){
                        let timeCurrentTime = moment(CurTime,"HH:mm:ss").tz("Asia/Jakarta").valueOf();
                        let onlyTimeAbsenPulang = moment(RekapData[iRekap].jam_pulang,"HH:mm:ss").tz("Asia/Jakarta").valueOf();

                        if(timeCurrentTime >= onlyTimeAbsenPulang){
                            RekapData[iRekap].denda_persen_masuk = parseFloat(dendaMasuk);
                            RekapData[iRekap].denda_persen_pulang = parseFloat(dendaPulang);
                        }else{
                            RekapData[iRekap].denda_persen_masuk = 0;
                            RekapData[iRekap].denda_persen_pulang = 0;
                            RekapData[iRekap].is_waiting = parseInt(1);
                        }
                    }else{
                        RekapData[iRekap].denda_persen_masuk = parseFloat(dendaMasuk);
                        RekapData[iRekap].denda_persen_pulang = parseFloat(dendaPulang);
                    }
                    

                    iRekap++;
                }

                response.send(SessionData.nip,true,"Berhasil mendapatkan rekap",RekapData,res);
            }).catch((e)=>{
                response.send('',false,"[301] "+e,{},res);
            });
            // console.log(RekapData);
            
        }).catch((e)=>{
            response.send('',false,"[302] "+e,{},res);
        });
    }).catch((e)=>{
        response.send('',false,"[303] "+e,{},res);
    });
}

exports.getRekapKegiatan = function (req,res){
    const { headers } = req;
    var params = {
        token:headers['x-token-access'],
        device_id:headers['x-device-id'],
        date_start:req.query.date_start,
        date_end:req.query.date_end,
        current_month:req.query.now,
        insert_date:req.query.insert_date
    }

    MDSession.getSessionInfo(params.token).then((SessionData)=>{
        MDRekap.getKegiatan(SessionData.nip,params.date_start,params.date_end,params.current_month,params.insert_date).then((RekapData)=>{
            response.send(SessionData.nip,true,"Berhasil mendapatkan rekap",RekapData,res);
        }).catch((e)=>{
            response.send('',false,"[201] "+e,{},res);
        });
    }).catch((e)=>{
        response.send('',false,"[202] "+e,{},res);
    });
}

exports.submitKegiatan = function (req,res) {

    const { headers } = req;
    var params = {
        token:headers['x-token-access'],
        device_id:headers['x-device-id'],
        judul:req.body.judul,
        keterangan:req.body.keterangan,
        foto_kegiatan:req.body.foto_kegiatan,
        foto_kegiatan_thumb:req.body.foto_kegiatan_thumb,
        foto_name:req.body.foto_name,
        thumb_name:req.body.thumb_name,
        id_kegiatan:req.body.id_kegiatan
    }

    MDSession.getSessionInfo(params.token).then((SessionData)=>{
        MDRekap.submitKegiatan(SessionData.nip,params).then((ResultSubmit)=>{
            if(ResultSubmit.success){
                response.send(SessionData.nip,true,"Berhasil submit kegiatan.",{},res);
            }else{
                response.send(SessionData.nip,false,"Gagal submit kegiatan.",{},res);
            }
        }).catch((err)=>{
            console.log(err);
            response.send(SessionData.nip,false,err,{},res);
        });
    }).catch((e)=>{
        response.send('',false,e,{},res);
    });
}